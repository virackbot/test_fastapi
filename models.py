from database import Base
from sqlalchemy import String, Boolean, Integer, Column, Text, Float


class Item(Base):
    __tablename__ = 'tblitems'
    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False, unique=True)
    description = Column(Text)
    price = Column(Float, nullable=False)
    on_offer = Column(Boolean, default=False)


def __repr__(self):
    return f'<Item name={self.name} description={self.description} price={self.price}>'
