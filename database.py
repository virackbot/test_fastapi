from sqlalchemy.orm import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker

engine = create_engine(
    'postgresql://postgres:pgs123@127.0.0.1:5432/item_db', echo=True)

Base = declarative_base()
SessionLocal = sessionmaker(bind=engine)
